/* 
   \\\\\\\\\\\\\\\\\\\\    5.SLEEP    ////////////////////
*/
const uint16_t ADC_SNOOZE_DELAY = 250; //250ms delay for each loop function (NOT used with delay() )

uint32_t lastAdcRead = 0;

////////// 5.1.DEEP SLEEP //////////
void initiateDeepSleep() {
  //We must inform the user, wait to debounce the button and then sleep.
  //if we sleep while power is pressed, we will wake up again.
  //shutdown display immediately
  shutDownDisplay();
  #ifdef STATUS_LED
    //pulse status led until the user releases the power button    
    bool value = false;
    while(digitalRead(POWER_BUTTON_PIN)) {
      value = !value;
      switchStatusLed(value);
      delay(500);
    }
  #endif
  #ifdef PRO
    //disable our LDO
    digitalWrite(ADC_LDO_EN_PIN,ADC_LDO_DISABLE);
  #endif
  disableSecondaryButtons();
  #ifdef SERIAL_IF
    Serial.flush();
  #endif
  delay(250);
  esp_deep_sleep_start();
}


////////// 5.2.LIGHT SLEEP //////////
void initiateLightSleep() {
  DEBUG_PRINTLN("shutting down ADC,dimming display");
  #ifdef PRO
    //disable our LDO
    digitalWrite(ADC_LDO_EN_PIN,ADC_LDO_DISABLE);
  #endif
  disableSecondaryButtons();
  lightSleep = true;
  wakeup = false;
  scale.powerOff();
  shutDownDisplay();
  #ifdef STATUS_LED  
    //keep status led on to inform the user that we are NOT on deep sleep
    switchStatusLed(true);
  #endif
  //https://github.com/espressif/esp-idf/issues/2070
  //btStop();
}

void wakeUpFromLightSleep() {
  #ifdef STATUS_LED
    switchStatusLed(false);
    delay(150);
    switchStatusLed(true);
    delay(150);
    switchStatusLed(false);
  #endif
  #ifdef PRO
    //disable our LDO
    digitalWrite(ADC_LDO_EN_PIN,ADC_LDO_ENABLE);
  #endif
  delay(50);
  enableSecondaryButtons();
  lightSleep = false;
  snooze = false;
  displayExtraRefreshInterval = 0;
  wakeup = false;
  scale.powerOn();
  wakeUpDisplay();
  lastActionMillis = millis();  
  //https://github.com/espressif/esp-idf/issues/2070
  //btStart();
}
