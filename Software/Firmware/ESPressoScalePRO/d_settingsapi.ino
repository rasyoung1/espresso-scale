/* 
   \\\\\\\\\\\\\\\\\\\\    3.SETTINGS API    ////////////////////
*/


//below are all the commands the user can read/write through BLE/Serial
String handleCommand(String cmd, String val) {
  bool getCommand = val.indexOf("get") >= 0; 
  if (cmd == "tare") {
    DEBUG_PRINTLN("tare command");
    beginTare = millis() + 50;
    tareType = 0;
    calibrateTare = false;
    saveWeight = true;      
  } else if (cmd == "calibrate") {
    if (!getCommand) {
      calibrateToUnits =  strtof(val.c_str(),NULL);
    }
  } else if (cmd == "reboot") {
    ESP.restart();
  } else if (cmd == "deepSleep") {
    deepSleepNow = true;
  } else if (cmd == "resetSettings") {
    settings.resetSettings();
    ESP.restart();
  } else if (cmd == "saveSettings") {
    settings.saveSettings();
  } else if (cmd == "startTimer") {
    scale.startTimer(true); //enable a manual timer
  } else if (cmd == "stopTimer") {
    scale.stopTimer();    
  } else if (cmd == "resetTimer") {
    scale.resetTimer();    
  } else if (cmd == "upgradeMode") {
    if (getCommand) {
      return String(upgradeMode);
    }    
  } else if (cmd == "wifiIP") {
    if (getCommand) {
      return wifiIP;
    }
  } else if (cmd == "voltage") {
    if (getCommand) {
      return String(vinVoltage);
    } 
  } else if (cmd == "resolution") {
    if (getCommand) {
      return String(resolutionLevel);
    } 
  } else if (cmd == "otaUpgrade") {
    if (getCommand) {
      return String(settings.otaUpgrade);
    } else {
      uint8_t value = atoi(val.c_str());
      if (value == 1) {
        //specifically check if 1, else =0 ignore all other values;
        settings.saveSettingByte("otaUpgrade",1);
        ESP.restart();
      } else {
        settings.saveSettingByte("otaUpgrade",0);
      }
    }
  } else if (cmd == "snoozeTimeout") {
    if (getCommand) {
      return String(settings.snoozeTimeout);
    } else {
      uint8_t value = atoi(val.c_str());
      settings.snoozeTimeout = value;
      settings.saveSettingByte("snoozeTimeout",value);
    }
  } else if (cmd == "lightSleepTimeout") {
    if (getCommand) {
      return String(settings.lightSleepTimeout);
    } else {
      uint8_t value = atoi(val.c_str());
      settings.lightSleepTimeout = value;
      settings.saveSettingByte("lightSleepTimeout",value);
    }
  } else if (cmd == "deepSleepTimeout") {
    if (getCommand) {
      return String(settings.deepSleepTimeout);
    } else {
      uint8_t value = atoi(val.c_str());
      settings.deepSleepTimeout = value;
      settings.saveSettingByte("deepSleepTimeout",value);
    }  
  } else if (cmd == "autoTare") {
    if (getCommand) {
      return String(settings.autoTare);
    } else {
      uint8_t value = atoi(val.c_str());
      settings.autoTare = value;
      scale.autoTare = value;
      settings.saveSettingByte("autoTare",value);
    }  
  } else if (cmd == "autoTareNegative") {
    if (getCommand) {
      return String(settings.autoTareNegative);
    } else {
      uint8_t value = atoi(val.c_str());
      settings.autoTareNegative = value;
      scale.autoTareNegative = value;
      settings.saveSettingByte("autoTareNegative",value);
    }  
  } else if (cmd == "autoStartTimer") {
    if (getCommand) {
      return String(settings.autoStartTimer);
    } else {
      uint8_t value = atoi(val.c_str());
      settings.autoStartTimer = value;
      scale.autoStartTimer = value;
      settings.saveSettingByte("autoStartTimer",value);
    }      
  } else if (cmd == "timerDelay") {
    if (getCommand) {
      return String(settings.timerDelay);
    } else {
      uint8_t value = atoi(val.c_str());
      scale.timerDelay = value*1000;
      settings.timerDelay = value;
      settings.saveSettingByte("timerDelay",value);
    }  

  } else if (cmd == "timerStartWeight") {
    if (getCommand) {
      return String(settings.timerStartWeight);
    } else {
      uint8_t value = atoi(val.c_str());
      settings.timerStartWeight = value;
      scale.timerStartWeight = (float)value/10.0;
      settings.saveSettingByte("timerStartWeight",value);
    }   
  } else if (cmd == "timerStopWeight") {
    if (getCommand) {
      return String(settings.timerStopWeight);
    } else {
      uint8_t value = atoi(val.c_str());
      settings.timerStopWeight = value;
      scale.timerStopWeight = (float)value;
      settings.saveSettingByte("timerStopWeight",value);
    }  
  } else if (cmd == "rocStartTimer") {
    if (getCommand) {
      return String(settings.rocStartTimer);
    } else {
      uint8_t value = atoi(val.c_str());
      settings.rocStartTimer = value;
      scale.rocStartTimer = (float)value/10.0;
      settings.saveSettingByte("rocStartTimer",value);
    }         
  } else if (cmd == "rocStopTimer") {
    if (getCommand) {
      return String(settings.rocStopTimer);
    } else {
      uint8_t value = atoi(val.c_str());
      settings.rocStopTimer = value;
      scale.rocStopTimer = (float)value/10.0;
      settings.saveSettingByte("rocStopTimer",value);
    }         
  } else if (cmd == "timerShowNegative") {
    if (getCommand) {
      return String(settings.timerShowNegative);
    } else {
      uint8_t value = atoi(val.c_str());
      settings.timerShowNegative = value;
      settings.saveSettingByte("timerShowNegative",value);
    }         
  } else if (cmd == "autoStopTimer") {
    if (getCommand) {
      return String(settings.autoStopTimer);
    } else {
      uint8_t value = atoi(val.c_str());
      settings.autoStopTimer = value;
      scale.autoStopTimer = value;
      settings.saveSettingByte("autoStopTimer",value);
    }      
  } else if (cmd == "scaleUnits") {
    if (getCommand) {
      return String(settings.scaleUnits);
    } else {
      uint8_t value = atoi(val.c_str());
      settings.scaleUnits = value;
      settings.saveSettingByte("scaleUnits",value);      
    }
  } else if (cmd == "zeroTracking") {
    if (getCommand) {
      return String((int)(scale.zeroTracking*100.0));
    } else {
      uint8_t value = atoi(val.c_str());
      settings.zeroTracking = value;
      scale.zeroTracking = (float)value/100.0;
      settings.saveSettingByte("zeroTracking",value);
    }    
  } else if (cmd == "zeroRange") {
    if (getCommand) {
      return String((int)(scale.zeroRange*100.0));
    } else {
      uint8_t value = atoi(val.c_str());
      settings.zeroRange = value;
      scale.zeroRange = (float)value/100.0;
      settings.saveSettingByte("zeroRange",value);
    }
  } else if (cmd == "fakeRange") {
    if (getCommand) {
      return String((int)(scale.fakeStabilityRange*100.0));
    } else {
      uint8_t value = atoi(val.c_str());
      settings.fakeRange = value;
      scale.fakeStabilityRange = (float)value/100.0;
      settings.saveSettingByte("fakeRange",value);
    }
  } else if (cmd == "stableWeightDiff") {
    if (getCommand) {
      return String((int)(scale.stableWeightDiff*100.0));
    } else {
      uint8_t value = atoi(val.c_str());
      settings.stableWeightDiff = value;
      scale.stableWeightDiff = (float)value/100.0;
      settings.saveSettingByte("stableWeightDiff",value);
    }    
  } else if (cmd == "sensitivity") {
    if (getCommand) {
      return String(settings.sensitivity);
    } else {
      uint8_t value = atoi(val.c_str());
      settings.sensitivity = value;
      scale.setSensitivity(value);
      settings.saveSettingByte("sensitivity",value);
    }
  } else if (cmd == "smoothing") {
    if (getCommand) {      
      return String(scale.getSmoothing());
    } else {
      uint8_t value = atoi(val.c_str());
      settings.smoothing = value;
      scale.setSmoothing(value);
      settings.saveSettingByte("smoothing",value);
    }
  } else if (cmd == "adcSpeed") {
    if (getCommand) {
      return String(scale.getSpeed());
    } else {
      uint8_t value = atoi(val.c_str());
      settings.adcSpeed = value;
      scale.setSpeed(value);
      settings.saveSettingByte("adcSpeed",value);
    }
  } else if (cmd == "decimalDigits") {
    if (getCommand) {
      return String(scale.decimalDigits);
    } else {
      uint8_t value = atoi(val.c_str());
      settings.decimalDigits = value;
      scale.decimalDigits = value;
      settings.saveSettingByte("decimalDigits",value);
    }
  } else if (cmd == "bleEnabled") {
    if (getCommand) {
      return String(settings.bleEnabled);
    } else {
      uint8_t value = atoi(val.c_str());
      settings.saveSettingByte("bleEnabled",value);
      ESP.restart();
    }
  } else if (cmd == "readSamples") {
    if (getCommand) {
      return String(settings.readSamples);
    } else {
      uint8_t value = atoi(val.c_str());
      settings.readSamples = value;
      settings.saveSettingByte("readSamples",value);
    }
  } else if (cmd == "slBtnPress") {
    if (getCommand) {
      return String(settings.slBtnPress);
    } else {
      uint8_t value = atoi(val.c_str());
      settings.slBtnPress = value;
      settings.saveSettingByte("slBtnPress",value);
    }
  } else if (cmd == "slMaxVIN") {
    if (getCommand) {
      return String(settings.slMaxVIN);
    } else {
      uint8_t value = atoi(val.c_str());
      settings.slMaxVIN = value;
      settings.saveSettingByte("slMaxVIN",value);
    }
  } else if (cmd == "batReadInterval") {
    if (getCommand) {
      return String(settings.batReadInterval);
    } else {
      uint8_t value = atoi(val.c_str());
      settings.batReadInterval = value;
      settings.saveSettingByte("batReadInterval",value);
      batReadInterval = settings.batReadInterval;
    }    
  } else if (cmd == "displayRotation") {
    if (getCommand) {
      return String(settings.displayRotation);
    } else {
      uint8_t value = atoi(val.c_str());
      settings.displayRotation = value;
      settings.saveSettingByte("displayRotation",value);
      displayRotation = settings.displayRotation;
      setRotation(displayRotation);
    }       
  } else if (cmd == "displayMaxBr") {
    if (getCommand) {
      return String(settings.displayMaxBr);
    } else {
      uint8_t value = atoi(val.c_str());
      settings.saveSettingByte("displayMaxBr",value);
      #ifdef LEDSEGMENT
        if (value > 15) {
          value = 15;
        }
      #endif
      settings.displayMaxBr = value;
      displayMaxBr = settings.displayMaxBr;
      brightenDisplay();
    }      
  } else if (cmd == "displayFps") {
    if (getCommand) {
      return String(settings.displayFps);
    } else {
      uint8_t value = atoi(val.c_str());
      settings.displayFps = value;
      settings.saveSettingByte("displayFps",value);
      displayRefreshInterval = 1000/value;
    }    
  } else if (cmd == "displayTopH") {
    if (getCommand) {
      return String(settings.displayTopH);
    } else {
      uint8_t value = atoi(val.c_str());
      settings.displayTopH = value;
      settings.saveSettingByte("displayTopH",value);
      displayTopSectionHeight = value;
      redrawDisplay = true;
    }
  } else if (cmd == "displayMainH") {
    if (getCommand) {
      return String(settings.displayMainH);
    } else {
      uint8_t value = atoi(val.c_str());
      settings.displayMainH = value;
      settings.saveSettingByte("displayMainH",value);
      displayMainSectionHeight  = value;
      redrawDisplay = true;
    }
  } else if (cmd == "displayBottomH") {
    if (getCommand) {
      return String(settings.displayBottomH);
    } else {
      uint8_t value = atoi(val.c_str());
      settings.displayBottomH = value;
      settings.saveSettingByte("displayBottomH",value);
      displayBottomSectionHeight = value;
      redrawDisplay = true;
    } 
  } else if (cmd == "bleNps") {
    if (getCommand) {
      return String(settings.bleNps);
    } else {
      uint8_t value = atoi(val.c_str());
      settings.bleNps = value;
      settings.saveSettingByte("bleNps",value);
      bleRefreshInterval = 1000/value;
    }      
  } else if (cmd == "graphInSection") {
    if (getCommand) {
      return String(settings.graphInSection);
    } else {
      uint8_t value = atoi(val.c_str());
      settings.graphInSection = value;
      settings.saveSettingByte("graphInSection",value);
      graphInSection = value;
      redrawDisplay = true;
    }      
  } else if (cmd == "tareButtonDelay") {
    if (getCommand) {
      return String(settings.tareButtonDelay);
    } else {
      uint16_t value = atoi(val.c_str());
      settings.tareButtonDelay = value;
      settings.saveSettingUShort("tareButtonDelay",value);
      tareButtonDelay = value;
    }   
  } else if (cmd == "calFactor") {
    if (getCommand) {
      return String(scale.getCalFactor());
    } else {
      float value = strtof(val.c_str(),NULL);
      uint32_t valLng = value*10;
      settings.saveSettingULong("calFactorULong",valLng); 
      scale.setCalFactor(value);
    }       
  } else if (cmd == "colorMain") {
    if (getCommand) {
      return String(colorMain);
    } else {
      uint32_t valLng = strtoul( val.c_str(), NULL, 10 );
      settings.saveSettingULong("colorMain",valLng); 
      colorMain = valLng;
      redrawDisplay = true;
    }   
  } else if (cmd == "colorTop") {
    if (getCommand) {
      return String(colorTop);
    } else {
      uint32_t valLng = strtoul( val.c_str(), NULL, 10 );
      settings.saveSettingULong("colorTop",valLng); 
      colorTop = valLng;
      redrawDisplay = true;
    }   
  } else if (cmd == "colorBottom") {
    if (getCommand) {
      return String(colorBottom);
    } else {
      uint32_t valLng = strtoul( val.c_str(), NULL, 10 );
      settings.saveSettingULong("colorBottom",valLng); 
      colorBottom = valLng;
      redrawDisplay = true;
    }   
  } else if (cmd == "colorMainBg") {
    if (getCommand) {
      return String(colorMainBg);
    } else {
      uint32_t valLng = strtoul( val.c_str(), NULL, 10 );
      settings.saveSettingULong("colorMainBg",valLng); 
      colorMainBg = valLng;
      redrawDisplay = true;
    }   
  } else if (cmd == "colorTopBg") {
    if (getCommand) {
      return String(colorTopBg);
    } else {
      uint32_t valLng = strtoul( val.c_str(), NULL, 10 );
      settings.saveSettingULong("colorTopBg",valLng); 
      colorTopBg = valLng;
      redrawDisplay = true;
    }   
  } else if (cmd == "colorBottomBg") {
    if (getCommand) {
      return String(colorBottomBg);
    } else {
      uint32_t valLng = strtoul( val.c_str(), NULL, 10 );
      settings.saveSettingULong("colorBottomBg",valLng); 
      colorBottomBg = valLng;
      redrawDisplay = true;
    }
  } else if (cmd == "wifiSSID") {
    if (getCommand) {
      return settings.wifiSSID;
    } else {
      settings.wifiSSID = val.c_str();
      DEBUG_PRINT("our new value for ssid is ");DEBUG_PRINTLN(val);
      settings.saveSettingString("wifiSSID",val.c_str()); 
    }
  } else if (cmd == "wifiPassword") {
    if (getCommand) {
      return settings.wifiPassword;
    } else {
      settings.wifiPassword = val.c_str();
      settings.saveSettingString("wifiPassword",val.c_str()); 
    }
  } else {
    //not a known command
    return "UNKNOWN";
  }
  return "OK";
}
