/* 
   \\\\\\\\\\\\\\\\\\\\    1.VARIABLES/CONSTS    ////////////////////
*/
////////// 1.1.BLE //////////

//const uint16_t BLE_REFRESH_INTERVAL = 350; //in millis
uint16_t bleRefreshInterval = 350;
//UPDATE: use settings option: bleNps (byte)

//random uuids, please do not use them in your final (commercial) product
//https://www.bluetooth.com/specifications/gatt/services
#define BLE_DEVICE_NAME "OSES"
#define BLE_SERVICE_UUID "b75d181d-e85e-11e8-9f32-f2801f1b9fd1"
#define BLE_CHARACTERISTIC_WEIGHT_TIMER_UUID "b75d2a9d-e85e-11e8-9f32-f2801f1b9fd1"
#define BLE_CHARACTERISTIC_SETTINGS_UUID "b75d2a9e-e85e-11e8-9f32-f2801f1b9fd3"


////////// 1.2.OTA UPDATE //////////
//           WARNING !!!          //
////////////////////////////////////
//If you want to use wifi OTA AND BLE, you must select a different partition scheme
//The sketch will not fit on the default one.
//For Arduino IDE go to Tools -> Partition Scheme -> Minimal SPIFFS (Large APPS with OTA)
uint8_t upgradeMode = 0; //1 if in upgrade mode
String wifiIP = "";
#ifdef OTA
  WebServer server(80);
  static uint8_t WIFI_CONNECTION_TIMEOUT = 30; //in seconds, if we do not connect within 30s, reboot
  
  const char* HTTP_SRV_LOGIN_HTML =
    #include "loginhtml.h"
  ;
  
  const char* HTTP_SRV_MAIN_HTML =
    #include "indexhtml.h"
  ;
#endif


////////// 1.3.DISPLAY //////////
//15fps (66ms)
//10fps (100ms) refresh if fine for 10SPS ADC speed if you need to show them all.
//8fps (125ms) is the threshold (IMO) for "look how fast it is" user reaction.
//4fps (250ms) too slow
//const uint8_t DISPLAY_REFRESH_INTERVAL = 100; //0 - 255
uint16_t displayRefreshInterval = 100;
//UPDATE: use settings option: displayFps (byte)

uint8_t displayMaxBr = 255; //255=100%. No real need for 100% backlight. Try lower values to save some juice.  Can be changed from the mobile app.

const uint8_t DISPLAY_INVERT_RB = 0; //your reds are blue ?? set this to 1

bool redrawDisplay = false; //on color changes or whenever you see fit
uint16_t displayExtraRefreshInterval = 0; //extra ms to match adc speed when in snooze mode. No need to refresh display 20times per second when our adc is down to 4sps.

#ifdef STATUS_LED
  const uint8_t STATUS_LED_PIN = 2;
  bool lastStatusLedValue = 0;  
#endif

void switchStatusLed(bool value) {
  #ifdef STATUS_LED
    if ( lastStatusLedValue != value ) {
      DEBUG_PRINT("status led => ");DEBUG_PRINTLN(value);
      digitalWrite(STATUS_LED_PIN,value);
      lastStatusLedValue = value;
    }
  #endif
}
void toggleStatusLed() {
  switchStatusLed(!lastStatusLedValue);
}



// HARDWARE SPI /// PLEASE READ ///
// If you select a library that does not accept SPI pointer, you will need to do the following edit
// If you are using library (like Adafruit's) that accepts SPI, do not bother reading.

//
//In PRO pcb the display is indeed connected to ESP32 hardware SPI but not the "default" one for Arduino IDE
//If you wish to use (and you should) hardware spi, please edit the pinout header file accordingly
//Location in windows10 :
//C:\Users\YOURUSERNAME\AppData\Local\Arduino15\packages\esp32\hardware\esp32\LIBRARY-VERSION\variants\BOARDNAME\pins_arduino.h
//so, for ESP32 WROOM (dev) , BOARDNAME = esp32 , edit the following const:
//static const uint8_t SS    = 15;
//static const uint8_t MOSI  = 13;
//static const uint8_t MISO  = 12;
//static const uint8_t SCK   = 14;
//You can get away with software SPI for maybe OLED, but TFT refresh will look awful
//

//#ifndef LEDSEGMENT

  const uint8_t DISPLAY_WIDTH = 128; // display width, in pixels
  const uint8_t DISPLAY_HEIGHT = 32; // display height, in pixels

  //define for the smallest text size (1), how many columns(letters) fit.
  //this will affect padding/justify and dynamic font size
  const uint8_t DISPLAY_COLS_TS1 = 20; //15 for 96px, 20 for 128px , 26 for 160px
  const uint8_t DISPLAY_COLS_TS2 = 9; //7 for 96px, 9 for 128px, 12 for 160px
  const uint8_t DISPLAY_COLS_TS3 = 6; //5 for 96px, 6 for 128px,
  const uint8_t DISPLAY_COLS_TS4 = 5; //4 for 96px, 5 for 128px, 6 for 160px
  
  const uint8_t DISPLAY_FONT_HEIGHT_TS1 = 8; //yoffset = (SECTION_HEIGHT - FONT_HEIGHT)/2

  const uint8_t DISPLAY_XOFFSET = 0; //generic offset
  //some more special offsets for each supported text size (1-4)
  const uint8_t DISPLAY_XOFFSET_TS1 = 0;
  const uint8_t DISPLAY_XOFFSET_TS2 = 0;
  const uint8_t DISPLAY_XOFFSET_TS3 = 0;
  const uint8_t DISPLAY_XOFFSET_TS4 = 0;

  //the drawing function will scale up/down automatically the text but if you wish, you can limit the max font size.
  //Your absolute (auto) max depends on section height but it might never be reached.
  const uint8_t DISPLAY_TOP_MAX_TEXT_SIZE = 1; 
  const uint8_t DISPLAY_MAIN_MAX_TEXT_SIZE = 2;
  const uint8_t DISPLAY_BOTTOM_MAX_TEXT_SIZE = 1;
      
  const uint8_t DISPLAY_MOSI_PIN = 13; //SDA if using I2C
  const uint8_t DISPLAY_CLK_PIN = 14; //SCL is using I2C
  const uint8_t DISPLAY_DC_PIN = 26;
  const uint8_t DISPLAY_CS_PIN = 15;
  const uint8_t DISPLAY_RESET_PIN = 27;
  
  const uint32_t COLOR_WHITE = 4294967295;
  const uint32_t COLOR_BLACK = 4278190080;

  //rotation and color options are also saved in eeprom and can be changed on the fly form the app !
  uint32_t colorMain = 4294967295;
  uint32_t colorTop = 4278190335;
  uint32_t colorBottom = 4294967295;
  uint32_t colorMainBg = 4278190080; 
  uint32_t colorTopBg = 4278190080;
  uint32_t colorBottomBg = 4278190080;
  
  uint8_t displayRotation = 0; //saved in eeprom, can be changed from app    
  
//#endif    

#ifdef SSD1306    
  #ifdef SSD1306_WIRE
    //Please read info above regarding SPI
    Adafruit_SSD1306 display(DISPLAY_WIDTH, DISPLAY_HEIGHT, &Wire, -1);
  #else
    //Ignore SPI notes above, we will initialize a secondary hardware SPI and pass it to the library
    SPIClass spiDisplay(HSPI);
    Adafruit_SSD1306 display(DISPLAY_WIDTH, DISPLAY_HEIGHT, &spiDisplay, DISPLAY_DC_PIN, DISPLAY_RESET_PIN, DISPLAY_CS_PIN);
  #endif
#endif


#ifdef SSD1331
  //Please read info above regarding SPI
  SSD_13XX display = SSD_13XX(DISPLAY_CS_PIN, DISPLAY_DC_PIN, DISPLAY_RESET_PIN);
#endif

#ifdef ST7735
  //Ignore SPI notes above, we will initialize a secondary hardware SPI and pass it to the library
  const uint8_t DISPLAY_BACKLIGHT_PIN = 25; // Display backlight pin
  
  const uint8_t DISPLAY_PWM_DIM_BRIGHTNESS = 1; // ~0.5%. drops consumption A LOT (~30mA-40mA).

  //init code
  #define DISPLAY_INITR INITR_MINI160x80
  //your blacks are white ??
  #define DISPLAY_INVERT true
  
  SPIClass spiDisplay(HSPI);
  Adafruit_ST7735 display = Adafruit_ST7735(&spiDisplay, DISPLAY_CS_PIN, DISPLAY_DC_PIN, DISPLAY_RESET_PIN);
#endif



#ifdef LEDSEGMENT
  const uint8_t LED_DIN_PIN = 23;
  const uint8_t LED_CLK_PIN = 18;
  const uint8_t LED_CS_PIN = 5;
  const uint8_t LED_COUNT = 5;
  const uint8_t LED_ADDR = 0;
//  const uint8_t LED_BRIGHTNESS_HIGH = 15; //0-15 --> we are using displayMaxBr
  const uint8_t LED_BRIGHTNESS_LOW = 1;
  LedControl display=LedControl(LED_DIN_PIN,LED_CLK_PIN,LED_CS_PIN,LED_COUNT);
#endif




////////// 1.4.SCALE //////////
#ifdef PRO
  const uint8_t ADC_LDO_EN_PIN = 21;
  const int ADC_LDO_ENABLE = 1; //high or low for enable ??? Check datasheet of your LDO.
  const int ADC_LDO_DISABLE = 0; //high or low for disable ??? Check datasheet of your LDO.
  
  const uint8_t ADC_PDWN_PIN = 5;
  const uint8_t ADC_DOUT_PIN = 19;
  const uint8_t ADC_SCLK_PIN = 18;
  const uint8_t ADC_GAIN0_PIN = 16;
  const uint8_t ADC_GAIN1_PIN = 17;
  const uint8_t ADC_SPEED_PIN = 22;
#endif

#ifdef LUNAR_ECLIPSE
  const uint8_t ADC_PDWN_PIN = 4;
  const uint8_t ADC_DOUT_PIN = 16;
  const uint8_t ADC_SCLK_PIN = 17;
  const uint8_t ADC_GAIN0_PIN = 12;
  const uint8_t ADC_GAIN1_PIN = 13;
  const uint8_t ADC_SPEED_PIN = 22;
#endif
const uint8_t ADC_A0_PIN = 0; //tied to GND = AIN1 in PRO
const uint8_t ADC_TEMP_PIN = 0; //tied to GND = AIN1 in PRO

SCALE scale = SCALE(ADC_PDWN_PIN, ADC_SCLK_PIN, ADC_DOUT_PIN, ADC_A0_PIN, ADC_SPEED_PIN, ADC_GAIN1_PIN, ADC_GAIN0_PIN, ADC_TEMP_PIN);
//SCALE scale = SCALE(ADC_PDWN_PIN, ADC_SCLK_PIN, ADC_DOUT_PIN);

const uint8_t INITIALISING_SECS = 5; //if our initialization time takes less than this value, keep waiting and taring.


////////// 1.5.BUTTONS //////////
//Note: T5 (IO12) is MTDI (strapping pin), MCU reads it on bootup. Be carefull.

//uncomment if you want to use the capacitive sensor of esp32
//#define TOUCH
//uncomment if you want to use any digital button (or external capacitive module with digital output)
#define BUTTON
#define INPUT_RESISTOR INPUT_PULLDOWN //standard button to IO/GND = INPUT_PULLUP , standard button to IO/VCC = INPUT_PULLDOWN. If you have external touch module, check its datasheet.
#define INTERRUPT_MODE RISING //INPUT_PULLUP -> FALLING or LOW, INPUT_PULLDOWN -> RISING OR HIGH


#ifdef TOUCH
// Larger value, more sensitivity
  const uint8_t TOUCH_POWER_THRESHOLD = 10;
  const uint8_t TOUCH_TARE_THRESHOLD = 10;
  const uint8_t TOUCH_SECONDARY1_THRESHOLD = 10;
#endif

#ifdef PRO
// middle pin of CN6 is GPIO12 or T5
// middle pin of CN7 is GPIO33 or T8
// middle pin of CN8 is GPIO32 or T9
  const uint8_t POWER_BUTTON_PIN = 12;
  const uint8_t TARE_BUTTON_PIN = 33;
  const uint8_t SECONDARY1_BUTTON_PIN = 32;
  #ifdef BUTTON
    //also, for wake up function we need to declare them as such (any other way ??? )
    //we only want to wake up if user touches power button
    #define POWER_BUTTON_PIN_RTC GPIO_NUM_12  
  #endif
#endif


#ifdef LUNAR_ECLIPSE
  const uint8_t POWER_BUTTON_PIN = 32;
  const uint8_t TARE_BUTTON_PIN = 33;
  const uint8_t SECONDARY1_BUTTON_PIN = 14;
  #ifdef BUTTON
    //also, for wake up function we need to declare them as such (any other way ??? )
    //we only want to wake up if user touches power button
    #define POWER_BUTTON_PIN_RTC GPIO_NUM_32  
  #endif
#endif





////////// 1.6.OTHER //////////
float roundToDecimal(double value, int dec)
{
  double mlt = powf( 10.0f, dec );
  value = roundf( value * mlt ) / mlt;
  return (float)value;
}


uint8_t batReadInterval = 30; //in seconds - defines how frequently we should read the voltage from our voltage divider.
uint32_t lastVinRead = 0;
float vinVoltage = 0.0;
String resolutionLevel = "";
uint32_t lastActionMillis = 0; 
bool snooze = false;
bool lightSleep = false;
bool wakeup = false;
bool calibrationMode = false;
bool initialising = false;
uint32_t beginTare = 0; //0 = do not tare, >0 tare when millis() = beginTare
byte tareType = 0;
bool calibrateTare = false;
bool saveWeight = false;
bool calibrating = false;
float calibrateToUnits = 0.0;
double gramsDbl = 0.0;
String gramsStr = "";
String timerStr = "";
float timerInSeconds=0.0;
bool deepSleepNow = false;
uint8_t graphInSection = 0;
bool resetWeightGraph = false;
